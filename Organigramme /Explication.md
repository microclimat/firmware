# Explication Organigramme

Nous emettons depuis chaque capteur afin d'avoir plusieurs mesures. Cela permet de faire une moyenne entre les mesures de 2 capteurs opposés et d'avoir un resultat au plus proche de la réalité possible.

Le calcul de la vitesse du vent se fera grâce à la formule suivante : 
Vvent = distance parcouru par l'onde / |(Tthèorique - Tmesurée)|

En suivant cette méthode, nous aurons donc 2 mesures de la vitesse du vent, une suivant l'axe des ordonnées et l'autre suivant l'axe des l'absices. Grâce à cela, nous pourrons retrouvé la direction du vent.